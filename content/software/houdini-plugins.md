---
author: "Egor Larionov"
title: "Houdini Plugins"
slug: "houdini-plugins"
date: 2022-05-31
description: "Plugins I wrote for SideFX Houdini."
draft: false
---

I developed the following plugins for [Houdini](https://www.sidefx.com/) to aid my research in
soft tissue simulation.
All binaries here are built from open source code available on [GitHub](https://github.com/elrnv).

# Installation


The following instructions demonstrate how to install Houdini plugins on various platforms.
Remember to replace the given Houdini version with the desired one.


## Linux

After downloading the appropriate plugin binary, simply drop it into your `~/houdini18.5/dso`
folder.  To make sure that Houdini sees the plugins, create a symlink to the
versioned library with
```
ln -s ~/houdini18.5/dso/vtkio.so.0.2.0 ~/houdini18.5/dso/vtkio.so
```
where `vtkio.so.0.2.0` should be substituted with one of the dynamic libraries provided below and
`houdini18.5` should correspond to the correct Houdini version.


## macOS

These instructions are similar to what is done on Linux.

After downloading the appropriate plugin binary, simply drop it into your
`~/Library/Preferences/houdini/18.5/dso` folder.  Make sure no other version of the same plugin
resides in the `~/LibraryPreferences/houdini/18.5/dso` directory to avoid conflicts.


## Windows

After downloading the appropriate zip file, unzip it into your `%UserProfile%\Documents\houdini18.5\dso`
folder.  Note that this will overwrite any existing versions of the same plugin you had installed
before, so make sure to back up any previous versions.


# I/O Extensions

The following plugins enable Houdini to import and export additional types of mesh files.
The code for these plugins is hosted on [GitHub](https://github.com/elrnv/hdkrs).


## VTK I/O

This plugin allows one to import and export Legacy [VTK](https://vtk.org) files (in binary format).
These files have support for primitive and point attributes in polygonal or tetrahedral meshes.

| Version | Build | Download Link |
|:-------:|:--------------|:--------------|
|**v0.3.0**|Linux 3.10 x86 64bit for Houdini 19.0 | **[<samp>vtkio.so.0.3.0</samp>](/software/x86/lin/vtkio.so.0.3.0)** (12MB)|
||macOS 12.3 x86 64bit for Houdini 19.0 | **[<samp>vtkio.0.3.0.dylib</samp>](/software/x86/mac/vtkio.0.3.0.dylib)** (7.9MB)|
||macOS 12.3 arm 64bit for Houdini 19.0 | **[<samp>vtkio.0.3.0.dylib</samp>](/software/arm/mac/vtkio.0.3.0.dylib)** (7.0MB)|
||Windows 10 x86 64bit for Houdini 19.0 | **[<samp>vtkio.0.3.0.zip</samp>](/software/x86/win/vtkio.0.3.0.zip)** (1.7MB)|
|**v0.2.0**|Linux 5.8 x86 64bit  for Houdini 18.5 | **[<samp>vtkio.so.0.2.0</samp>](/software/vtkio.so.0.2.0)** (14MB)|
||macOS 10.15 x86 64bit for Houdini 18.5 | **[<samp>vtkio.0.2.0.dylib</samp>](/software/vtkio.0.2.0.dylib)** (10MB)|
||Windows 10 x86 64bit for Houdini 18.5 | **[<samp>vtkio.0.2.0.zip</samp>](/software/vtkio.0.2.0.zip)** (1.2MB)|
|**v0.1.0**|Linux 5.0 x86 64bit for Houdini 17.5 | **[<samp>vtkio.so.0.1.0</samp>](/software/vtkio.so.0.1.0)** (6.4MB)|
||macOS 10.14 x86 64bit for Houdini 17.5 | **[<samp>vtkio.0.1.0.dylib</samp>](/software/vtkio.0.1.0.dylib)** (5.3MB)|
||Windows 10 x86 64bit for Houdini 17.5 | **[<samp>vtkio.0.1.0.zip</samp>](/software/vtkio.0.1.0.zip)** (0.7MB)|


## Msh I/O

An importer (exporting not yet supported) for [Gmsh](https://gmsh.info) version 4.1 files into Houdini.

| Build Version | Download Link |
|:--------------|:--------------|
|Linux 3.10 x86 64bit version **0.1.1** for Houdini 19.0 | **[<samp>mshio.so.0.1.1</samp>](/software/x86/lin/mshio.so.0.1.1)** (4.0MB)|
|macOS 12.3 x86 64bit version **0.1.1** for Houdini 19.0 | **[<samp>mshio.0.1.1.dylib</samp>](/software/x86/mac/mshio.0.1.1.dylib)** (2.5MB)|
|macOS 12.3 arm 64bit version **0.1.1** for Houdini 19.0 | **[<samp>mshio.0.1.1.dylib</samp>](/software/arm/mac/mshio.0.1.1.dylib)** (2.2MB)|
|Windows 10 x86 64bit version **0.1.1** for Houdini 19.0 | **[<samp>mshio.0.1.1.zip</samp>](/software/x86/win/mshio.0.1.1.zip)** (0.5MB)|


## Obj I/O

This is a slightly faster (at the time of this writing) alternative for importing and exporting
Wavefront OBJ files in Houdini compared to the builtin version.

| Build Version | Download Link |
|:--------------|:--------------|
|Linux 3.10 x86 64bit version **0.1.1** for Houdini 19.0 | **[<samp>objio.so.0.1.1</samp>](/software/x86/lin/objio.so.0.1.1)** (4.0MB)|
|macOS 12.3 x86 64bit version **0.1.1** for Houdini 19.0 | **[<samp>objio.0.1.1.dylib</samp>](/software/x86/mac/objio.0.1.1.dylib)** (2.9MB)|
|macOS 12.3 arm 64bit version **0.1.1** for Houdini 19.0 | **[<samp>objio.0.1.1.dylib</samp>](/software/arm/mac/objio.0.1.1.dylib)** (2.2MB)|
|Windows 10 x86 64bit version **0.1.1** for Houdini 19.0 | **[<samp>objio.0.1.1.zip</samp>](/software/x86/win/objio.0.1.1.zip)** (0.5MB)|


# Implicits SOP

*Implicits* is a SOP that computes various types of implicit surfaces. The current implementation
takes a collection of points (for example on a grid) on the first input and another polygon mesh on
the second input, and computes an implicit function on the points in the first input representing
the mesh provided by the second input. The supported implicit functions include [Hermite Radial
Basis Functions](http://rodolphe-vaillant.fr/?e=12) and [Moving Least Squares
Surfaces](http://graphics.berkeley.edu/papers/Shen-IAI-2004-08/). Actual documentation is
warranted, and is planned to be released with the code.

| Build Version | Download Link |
|:--------------|:--------------|
| Linux 5.0 x86 64bit dynamic library version **0.1.1** | **[<samp>sop_implicits.so.0.1.1</samp>](/software/sop_implicits.so.0.1.1)** (7.8MB)|


# Compatibility

These plugins have to be compatible with the right version of Houdini
as well as the operating system (OS). The given OS version numbers above show versions
that were tested to work. Most likely a wider range of OS versions will work.


# Acknowledgments

These plugins depend on many third-party open source libraries, which means this work would not be
possible without all of the open source contributors involved in developing the supporting software.
I would also like to thank the folks at SideFX for building a powerful tool like Houdini, providng
support, stellar documentation and loads of online learning resources.
