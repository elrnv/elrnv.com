+++
author = "Egor Larionov"
categories = ["notes", "quantum information", "channel capacities", "iqc"]
date = 2012-09-15T01:40:00Z
description = ""
draft = false
slug = "notes-on-continuity-of-channel-capacities"
tags = ["quantum information", "channel capacities", "iqc"]
title = "Notes on Continuity of Quantum Channel Capacities"
thumb = "/img/researchnotes_thumbnail.png"
pdf = "/quantum/researchnotes.pdf"
+++

I spent the summer of 2012 as an undergraduate research assistant working under Debbie Leung at the [Institute for Quantum Computing](http://iqc.uwaterloo.ca). Since then I have decided to pursue a career in computer graphics, although quantum information remains one of my amateur interests. I have compiled the notes I took during this research semester into one document:

[![Research notes on continuity of channel capacities](/img/researchnotes_thumbnail.png)](/quantum/researchnotes.pdf)

There is little organization within these notes, however they summarize some of the necessary background to start the study of quantum information, and include a few proofs of theorems from various Quantum Information books, as well as a few new ideas aimed at proving the continuity of a particular quantum capacity. These notes aren't complete in any sense, because unfortunately I didn't get a chance to typeset all of my work.
