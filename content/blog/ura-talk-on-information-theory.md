+++
author = "Egor Larionov"
categories = ["notes", "quantum information", "information theory"]
date = 2013-03-03T10:22:00Z
description = ""
draft = false
slug = "ura-talk-on-information-theory"
tags = ["quantum information", "information theory"]
title = "Undergraduate Research Seminar Talk on Information Theory"

thumb = "/quantum/seminartalk_thumb.png"
pdf = "/quantum/seminartalk.pdf"
+++

At the end of my undergraduate research semester in the summer of 2012 with [IQC](http://iqc.uwaterloo.ca), I compiled a talk on classical and quantum information. I tried to start with an introduction to classical information and transition into quantum information on a very basic level. This talk should be appropriate for all audiences with a basic background in linear algebra. These notes on Classical and Quantum Information are somewhat incomplete, and will remain so, unless I give this talk again some day:

[![Seminar talk on Information Theory](/quantum/seminartalk_thumb.png)](/quantum/seminartalk.pdf)
