+++
author = "Egor Larionov"
categories = ["notes", "shape reconstruction", "level set", "cs870", "motion tracking", "image processing"]
date = 2013-11-25T22:58:00Z
description = "Course presentation slides for Numerical Algorithms and Image Processing (CS870)"
draft = false
slug = "slides-for-cs870-talks"
tags = ["shape reconstruction", "level set", "cs870", "motion tracking", "image processing"]
title = "Slides for Papers on Motion Tracking and Surface Reconstruction"
type = "notes"
institution = "University of Waterloo"
thumb = "/cs870/srslides_thumb.png"
+++

CS 870 is a course at the University of Waterloo that briefly covers numerical PDE solutions, especially to the level set PDE developed by Stanley Osher and James A. Sethian in a paper called "[Fronts propagating with curvature-dependent speed](https://math.berkeley.edu/~sethian/Papers/sethian.osher.88.pdf)" from 1988. I presented two papers for this class (with links to slides):

* [Implicit Shape Reconstruction Using a Level Set Method](/cs870/srslides.pdf) (2000)
This paper presents a level-set based method to tightly wrap a 3D surface around a set of data points.
* [Level Set Approach for Motion Detection and Tracking](/cs870/trslides.pdf) (1998)
Here, the authors extend the geodesic active contour model, which segments images, to detect and track moving objects in motion sequences.

Note that the background for the level set method itself is omitted.
