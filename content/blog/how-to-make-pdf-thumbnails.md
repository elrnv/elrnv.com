+++
author = "Egor Larionov"
date = 2016-03-22T03:16:55Z
description = ""
draft = true
slug = "how-to-make-pdf-thumbnails"
title = "How to make PDF thumbnails"

+++

Use imagemagik with this command:
```
convert -thumbnail x300 -background white -alpha remove file.pdf[0] file_thumbnail.png
```
