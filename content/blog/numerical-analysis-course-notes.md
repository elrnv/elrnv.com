+++
author = "Egor Larionov"
categories = ["numerical analysis", "notes", "UW"]
date = 2013-09-16T22:05:00Z
description = "Course notes for Numerical Analysis (AM740/CM770/CS770)"
draft = false
slug = "numerical-analysis-course-notes"
tags = ["numerical analysis", "course notes", "UW"]
title = "Notes on Numerical Analysis"
thumb = "/img/cs770notes_thumbnail.png"
type = "notes"
pdf = "/cs770/cs770notes.pdf"
+++

I decided to typeset the course notes for the Numerical Analysis course (AM740/CM770/CS770) at the University of Waterloo for fall 2013 taught by Hans De Sterck. Use them at your own risk, since they may contain errors:

[![Notes on Fourier analysis on ToF transient imaging](/img/cs770notes_thumbnail.png)](/cs770/cs770notes.pdf)
