---
author: "Egor Larionov"
date: 2017-03-09T18:30:26Z
description: ""
draft: true
slug: "archlinux-setup"
title: "ArchLinux Setup"
---

# Introduction

The following ArchLinux installation guide is more or less an accumulation of excerpts from the
[ArchWiki](https://wiki.archlinux.org/index.php/Installation_guide) that are relevant to my use
cases along with various relevant articles with solutions to various problems I have previously
encountered with the installation.

First downlaod and boot an ArchLinux iso image from [here](https://www.archlinux.org/download/).
Don't forget to verify the download integrity by running `md5` or `shasum` on the iso image.

No action required to set up keyboard layout or font, the defaults are suitable.

Internet: wired, enabled by default. For wireless check [Wireless network configuration](https://wiki.archlinux.org/index.php/Wireless_network_configuration)

Enable time/date network synchronization via

```
# timedatectl set-ntp true
```

# Partitioning

We build a UEFI/GPT partitioning layout created with `parted`, which should be already available.
Refer to the [Parted](https://wiki.archlinux.org/index.php/Parted) wiki for more details.

First run

```
# fdisk -l
```

to list available drives on the machine. Then select the drive to partition (e.g. `/dev/sda`) with

```
# parted /dev/sda
```

This puts us into a GNU Parted environment, which should indicate which device we are working on
with something like `Using /dev/sda`.
Now create a GUID partition table (GPT) with

```
(parted) mklabel gpt
```

The command to make partitions looks like `mkpart`*`part-type fs-type start end`*. We will create the
recommended UEFI/GPT partition layout from [the
wiki](https://wiki.archlinux.org/index.php/Partitioning#Example_layouts).

For the boot partition run

```
(parted) mkpart primary fat32 1MiB 551MiB
(parted) set 1 esp on
```

where `esp` stands for EFI system partition.

For systems with smaller storage (like VirtualBox instance) it's probably best to use one monolithic
partition and use a swapfile instead of a dedicated swap partition. To do this, run

```
(parted) mkpart primary ext4 551MiB 100%
```

For proper systems with enough space, we can run

```
(parted) mkpart primary ext4 551MiB 20.5GiB
(parted) mkpart primary linux-swap 20.5GiB 24.5GiB
(parted) mkpart primary ext4 24.5GiB 100%
```

for a separate 20 GiB root (`/`) partition, 4 GiB of swap and the rest for `/home`. (For reference, 1 GiB =
1.0737 GB). The motivation for separating out a home partition is for safety of upgrading the OS.
You may want to create a separate `/usr` partition to share between multiple OSes and even a
separate `/tmp` partition to have a faster (but less reliable) file system. [Reference
article](https://www.lifewire.com/do-you-need-home-partition-2202048) and relevant [ArchWiki
article](https://wiki.archlinux.org/index.php/Partitioning#Partition_scheme)

Finally once finished, simply run

```
(parted) quit
```

to exit `parted` and format the created partitions with `mkfs.<fs-type> <partition-name>` command. For example to format the second partition on the `sda` device with `ext4` file system, do

```
# mkfs.ext4 /dev/sda2
```

To format the boot (EFI system) partition, run

```
# mkfs.fat -F32 /dev/sda1
```

assuming it's the first partition created on `sda`.

If a swap partition was made, format it with

```
# mkswap /dev/sdX?
# swapon /dev/sdX?
```

where `?` refers to the partition number of the swap partition and `sdX` to the appropriate device
name.

Finally, we temporarily mount the root partition to `/mnt` with

```
mount /dev/sda2 /mnt
```

along with any other partitions we created. For example to mount the boot (EFI) partition, run

```
mount /dev/sda1 /mnt/efi
```


# Installation

Optionally modify the `/etc/pacman.d/mirrorlist` to put the closest (geographically) mirrors first,
and possibly also prioritize `https`. This may make the installation faster.

To bootstrap the system at the given mount points, run

```
# pacstrap /mnt base
```

This took 40 seconds with VirtualBox at the time of this writing, so it should be pretty fast.


# Configuration
 
Generate `fstab` file with

```
# genfstab -U /mnt >> /mnt/etc/fstab
```

Change root into the new system

```
# arch-chroot /mnt
```

To set up the timezone and hardware clock, run

```
# ln -sf /usr/share/zoneinfo/Canada/Pacific /etc/localtime
# hwclock --systohc
```

Uncomment `en_US.UTF-8 UTF-8` (UTF-8 is preferred) in `/etc/locale.gen` (and others if needed) and run

```
# locale-gen
```

to generate the locales. Create a file `/etc/locale.conf` with a single line reading

```
LANG=en_US.UTF-8
```

## Networking

Create a file `/etc/hostname` with the desired hostname like `arch` or something.

Edit `/etc/hosts` and add

```
127.0.0.1	localhost
::1		localhost
127.0.1.1	arch.localdomain	arch
```

where `arch` is the chosen hostname that needs to match what's in `/etc/hostname`.

To make sure that networking is up and running at system start, we need to enable `dhcpcd` by running

```
systemctl enable dhcpcd
```


## Password

Set the root password with

```
# passwd
```

## Bootloader

Install GRUB boot loader and microcode package with

```
# pacman -S grub intel-ucode
```
(use `amd-ucode` for AMD processors).

To install grub, make a new directory (if it doesn't exist) at `/boot/efi` and run the following:

```
# grub-install --efi-directory=/efi --bootloader-id=GRUB
```

Note that `sda` is the device name, NOT the partition name like `sda1`.

Configure grub (this install microcode updates so long as the `-ucode` package is installed) by running

```
# grub-mkconfig -o /boot/grub/grub.cfg
```

## Necessary Applications

The above section leaves the installation fairly bare. To get basic usability we need to install the
following

```
# pacman -S sudo fish patch fakeroot vim ripgrep ncdu keychain boost clang
```

To make sure `vim` is started when typing `vi`, just sym link it:

```
# mv /bin/vi /bin/vi_legacy
# ln -s /bin/vim /bin/vi
```

## VirtualBox

This section has instructions specific to VirtualBox installations, make sure you complete these
before rebooting into the new system.

It has been noted [here](https://wiki.archlinux.org/index.php/VirtualBox#Installation_in_EFI_mode)
that VirtualBox can't find the EFI bootloader unless it's at `/efi/EFI/BOOT/BOOTX64.EFI`. To fix
this, simply copy/move the bootloader to that location by running

```
cp /efi/EFI/GRUB/grubx64.efi /efi/EFI/BOOT/BOOTX64.EFI
```

To improve the VirtualBox experience install the following libraries:

```
sudo pacman -S virtualbox-guest-{modules-arch,utils}
```

This concludes the basic installation of ArchLinux. We can now reboot and hope it all worked.


# Users and Groups

Add an admin user with

```
# useradd -m -G wheel -s /usr/bin/fish egor
```

Enable users in the `wheel` group in the sudoers list by editing `/etc/sudoers` appropriately.
From now on I assume that the setup will be done through this new user.


# Desktop environments

For VirtualBox we probably don't need a display manager, so we'll install just the basics:

```
# sudo pacman -S xorg-server xorg-xinit nano-fonts urxvt awesome
```

We will use Awesome as the window manager though even with VirtualBox to make it easier to run
regular desktop apps.  We will use the
[urxvt](https://wiki.archlinux.org/index.php/Rxvt-unicode#Configuration) terminal emulator because
it has great performance, is customizable and supports unicode.

First we need a custom `xinit` config to start Awesome (since we dont have a display manager). Copy
the default config:

```
# sudo cp /etc/X11/xinit/xinitrc ~/.xinitrc
```

and change the last few lines that start up some applications with just `exec awesome` (Note the
last line must be the `exec` line).

Then we will configure Awesome:

```
# mkdir -p ~/.config/awesome
# cp /etc/xdg/awesome/rc.lua ~/.config/awesome/.
```

Then change the default terminal and editor to `urxvt` and `vim` respectively in that lua config
file.

# Applications

Common programs and development tools:

```
# sudo pacman -S cmake git firefox glu
```

Note that `glu` is needed for houdini.

# Workspace Config

The remaining notes in this section describe how to setup my personalized scripts and environment.

To get the configurations for git, vim and fish on a VirtualBox we need to mount vm-share:

```
# mkdir ~/vm-share
# mount -t vboxsf vm-share ~/vm-share
# pushd ~/vm-share
# cp .vimrc .gitconfig ~/.
# cp config.fish ~/.config/fish/.
# mkdir -p ~/.vim/bundle
# cp navigation.vim ~/.vim/.
# popd
```

Make sure to update the `.vimrc` with special configs for the machine.
At the time of this writing I still use vundle, so we need to clone Vundle.vim and install the
plugins:

```
# git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
```

The open vim and run `:PluginInstall` to install the plugins.


# Notes and Troubleshooting
- Make sure that Arch Boots in UEFI mode by doing `ls /sys/firmware/efi` to make sure it exists. If it doesn't and you're sure the motherboard supports UEFI, check BIOS settings to make sure it tries to boot the USB in UEFI mode first.

- Follow https://www.pckr.co.uk/arch-grub-mkconfig-lvmetad-failures-inside-chroot-install/ to get rid of lvmetad warnings (optional)

- Software repos are stored in `/etc/pacman.conf`
- Update changes to pacman with `pacman -Sy`

- Pacman understands regex:

```
sudo pacman -S virtualbox-guest-modules-arch virtualbox-guest-utils
```
becomes
```
sudo pacman -S virtualbox-guest-{modules-arch,utils}
```

- Run `sudo pacman-optimize` to optimize pacman
- Run `pacman -R package_name` to remove packages
- Run `pacman -Rns package_name` to remove package and its configs (`n`) and dependencies (`s`)
- Run `pacman -Rsc package_name` to force remove all packages that rely on the dependencies (DANGEROUS)
- Run `pacman -Sc` to remove old uninstalled packages
- Run `paccache -r` to purch all cached versions of packages leaving 3 most recent versions.

- When using custom shells insure it's in `/etc/shells`
