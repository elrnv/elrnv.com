+++
author = "Egor Larionov"
categories = ["about"]
date = 2020-01-28
description = ""
draft = false
slug = "about"
tags = ["about", "interests", "cv", "overview"]
title = "About Me"
menu  = "main"
+++

I am currently a researcher at Meta Reality Labs working on physically based animation.
I did my PhD on [simulation problems relating to contact and
friction](/phd/egor_larionov_thesis.pdf) at the University of British Columbia supervised
by [Dinesh K. Pai](https://sensorimotor.cs.ubc.ca/pai/).  During my Master's, I studied fluid
simulation at the University of Waterloo supervised by [Christopher Batty](https://cs.uwaterloo.ca/~c2batty/).
In undergrad I majored in Pure Mathematics and Computer Science with a minor in
Physics, also at the University of Waterloo.  I am interested in problems relating to the simulation of
natural phenomena, and how they can influence our experience in the virtual world.

I am also passionate about open source. I have written and contributed to a number of open source
projects, which can be found on [github](https://github.com/elrnv) or
[gitlab](https://gitlab.com/elrnv).  My goal in open source is to improve the quality of software
tools available for creators and researchers.  In my field, I believe that code quality has a
significant impact on research progress and quality of results.

<img class="imgborder alignright" src="/img/iceland_crater.jpg" alt="Standing on the Hverfjall crater in northern Iceland">

This is me on the Hverfjall volcanic crater with [lake My&#769;vatn](https://en.wikipedia.org/wiki/M%C3%BDvatn) in the background.

I'm a big fan of climbing, mountain biking, canoeing and travelling.
Otherwise, I spend the majority of my time working indoors, hunched in front of a computer.
