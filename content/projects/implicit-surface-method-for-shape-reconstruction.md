+++
author = "Egor Larionov"
categories = ["course", "project"]
date = 2014-02-06T08:47:00Z
description = "Course project for Numerical Algorithms and Image Processing (CS870)"
draft = false
slug = "implicit-surface-method-for-shape-reconstruction"
tags = ["implicit surface", "shape reconstruction", "level set", "matlab"]
title = "Implicit Surface Method for Shape Reconstruction"

thumb = "/cs870/cs870proj_thumb.png"
institution = "University of Waterloo"
pdf = "/cs870/cs870proj.pdf"
code = "/cs870/cs870proj.tar.gz"
+++

A few months ago I wrote an implementation of an implicit surface method for shape reconstruction
developed, in part, by H. Zhao, S. Osher, B. Merriman, and M. Kang, in their paper (2000) titled
["Implicit and Non-parametric Shape Reconstruction from Unorganized
Data"](https://www.sciencedirect.com/science/article/pii/S1077314200908750).
This paper describes a level-set method for reconstructing a surface given a collection of points,
curves and surface patches. My implementation deals with points alone and it is written entirely for
MATLAB. You can download the **[code and examples of generated images
here](/cs870/cs870proj.tar.gz)**. This download package also includes a brief
11 page writeup, explaining the details of the method and implementation. Alternatively you can look
at the **[writeup](/cs870/cs870proj.pdf)** directly. Note that the algorithm at
the end, used to compute the minimum local feature size is incorrect.
