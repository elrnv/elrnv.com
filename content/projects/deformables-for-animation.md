+++
author = "Egor Larionov"
authors = ["Uri M. Ascher", "Egor Larionov", "Seung Heon Sheen", "Dinesh K. Pai"]
date = 2022-01-01
journal = "Journal of Computational Dynamics"
linktitle = "Deformables for animation"
draft = false
slug = "deformables-for-animation"
title = "Simulating deformable objects for computer animation: a numerical perspective"
thumb = "/img/alsp21_thumbnail.png"
categories = ["publication"]
pdf = "https://arxiv.org/pdf/2103.01891.pdf"
+++

We examine a variety of numerical methods that arise when considering dynamical systems in the
context of physics-based simulations of deformable objects.  Such problems arise in various
applications, including animation, robotics, control and fabrication.  The goals and merits
of suitable numerical algorithms for these applications are different from those of typical
numerical analysis research in dynamical systems.  Here the mathematical model is not fixed *a
priori* but must be adjusted as necessary to capture the desired behaviour, with an emphasis on
effectively producing lively animations of objects with complex geometries.  Results are often judged
by how realistic they appear to observers (by the "eye-norm") as well as by the efficacy of the
numerical procedures employed.  And yet, we show that with an adjusted view numerical analysis and
applied mathematics can contribute significantly to the development of appropriate methods and their
analysis in a variety of areas including finite element methods, stiff and highly oscillatory ODEs,
model reduction, and constrained optimization.

| | |
| ----: | :---- |
| Paper | [PDF](https://arxiv.org/pdf/2103.01891.pdf) |
| Project Page | [arXiv](https://arxiv.org/abs/2103.01891), [JCD](https://www.aimsciences.org/article/doi/10.3934/jcd.2021021) |
| | |

## BibTeX

```
@article{alsp21,
    title = {
        Simulating deformable objects for computer animation:
        A numerical perspective
    },
    author = {
        Uri M. Ascher and
        Egor Larionov and
        Seung Heon Sheen and
        Dinesh K. Pai
    },
    year = {2021},
    journal = {Journal of Computational Dynamics},
    volume = {0},
    number = {},
    pages = {-},
}
```
