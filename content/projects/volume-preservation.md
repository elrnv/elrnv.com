+++
author = "Egor Larionov"
authors = ["Seung Heon Sheen", "Egor Larionov",  "Dinesh K. Pai"]
date = 2021-10-04
journal = "Proceedings of the ACM on Computer Graphics and Interactive Techniques"
linktitle = "Volume Preservation"
draft = false
slug = "volume-preserving-simulation-of-soft-tissue-with-skin"
title = "Volume Preserving Simulation of Soft Tissue with Skin"
thumb = "/img/volume_thumb.jpg"
categories = ["publication"]
pdf = "https://arxiv.org/pdf/2109.01170.pdf"
video = "https://www.youtube.com/watch?v=avHcilUto5A"
+++

![Volume Preserving Simulation Teaser](/img/volume_teaser.png)

Simulation of human soft tissues in contact with their environment is essential in many fields, including
visual effects and apparel design. Biological tissues are nearly incompressible. However, standard methods
employ compressible elasticity models and achieve incompressibility indirectly by setting Poisson’s ratio to
be close to 0.5. This approach can produce results that are plausible qualitatively but inaccurate quantatively.
This approach also causes numerical instabilities and locking in coarse discretizations or otherwise poses
a prohibitive restriction on the size of the time step. We propose a novel approach to alleviate these issues
by replacing indirect volume preservation using Poisson’s ratios with direct enforcement of zonal volume
constraints, while controlling fine-scale volumetric deformation through a cell-wise compression penalty.
To increase realism, we propose an epidermis model to mimic the dramatically higher surface stiffness on
real skinned bodies. We demonstrate that our method produces stable realistic deformations with precise
volume preservation but without locking artifacts. Due to the volume preservation not being tied to mesh
discretization, our method also allows a resolution consistent simulation of incompressible materials. Our
method improves the stability of the standard neo-Hookean model and the general compression recovery in
the Stable neo-Hookean model.


| | |
| ----: |:---- |
| Paper | [PDF](https://arxiv.org/pdf/2109.01170.pdf) |
| Project Page | [ACM](https://dl.acm.org/doi/abs/10.1145/3480143) |
| Video   | [YouTube](https://www.youtube.com/watch?v=avHcilUto5A) |
| | |

## BibTeX

```
@article{slp21,
    author = {Sheen, Seung Heon and Larionov, Egor and Pai, Dinesh K.},
    title = {Volume Preserving Simulation of Soft Tissue with Skin},
    year = {2021},
    issue_date = {September 2021},
    publisher = {Association for Computing Machinery},
    address = {New York, NY, USA},
    volume = {4},
    number = {3},
    url = {https://doi.org/10.1145/3480143},
    doi = {10.1145/3480143},
    journal = {Proc. ACM Comput. Graph. Interact. Tech.},
    month = sep,
    articleno = {32},
    numpages = {23},
    keywords = {finite element method, incompressibility, neo-Hookean elasticity, soft-tissue simulation, volume constraint}
}
```
