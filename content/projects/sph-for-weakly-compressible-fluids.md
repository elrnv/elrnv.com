+++
author = "Egor Larionov"
categories = ["course", "project"]
institution = "University of Waterloo"
date = 2015-02-09T05:53:02Z
description = "Course project for Physics-Based Animation (CS888)"
draft = false
slug = "sph-for-weakly-compressible-fluids"
tags = ["sph", "weakly compressible", "particle-based"]
title = "SPH for Weakly Compressible Fluids"
thumb = "/cs888/thumb.png"
pdf = "/cs888/cs888proj.pdf"
video = "https://www.youtube.com/embed/zRr84SF6FZw"
code = "https://github.com/elrnv/sph-cpp"
+++

In winter 2014, I took a course on physically-based simulation, and had the opportunity to work on a fluid simulator project. I finally found some time to organize my work and upload it to **[GitHub](https://github.com/elrnv/sph-cpp)**. The project outlines two notable methods in fluid simulation using [Smoothed Particle Hydrodynamics](http://en.wikipedia.org/wiki/Smoothed-particle_hydrodynamics) (SPH):

 * ["Particle-Based Fluid Simulation for Interactive Applications" by M. Muller, D. Charypar and M. Gross](http://matthias-mueller-fischer.ch/publications/sca03.pdf)
 * ["Weakly Compressible SPH for Free Surface Flows" by M. Becker, M. Teschner](http://cg.informatik.uni-freiburg.de/publications/2007_SCA_SPH.pdf)

A partial implementation of the [Implicit Incompressible SPH](http://cg.informatik.uni-freiburg.de/publications/2013_TVCG_IISPH.pdf) method developed by M. Ihmsen et al. can also be found in the codebase. I'm not planning to finish it, but I *can* be convinced otherwise. 

The initial idea of the project was to create a functioning SPH solver. However, after learning about the large variety of different approaches to SPH, I decided to write a more generic application which can simulate different types of SPH fluids simultaneously. For instance the current implementation allows two different SPH fluids (from the two bullets above) to interact.

I also wrote a SIGGRAPH style **[report](/cs888/cs888proj.pdf)** explaining what I've learned about SPH:
[![SIGGRAPH style report](/img/cs888proj_thumbnail.png)](/cs888/cs888proj.pdf)

Lastly, I uploaded a small **[video](https://www.youtube.com/embed/zRr84SF6FZw)** demonstrating the application on youtube:
<iframe width="420" height="315" src="https://www.youtube.com/embed/zRr84SF6FZw" frameborder="0" allowfullscreen></iframe>

I'm always happy to get feedback and/or answer questions, so feel free to add a comment or fire me an email. Fluid simulation is a fascinating topic, and my main research interest, at the moment.
