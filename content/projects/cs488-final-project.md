+++
author = "Egor Larionov"
categories = ["course", "project"]
date = 2012-12-07T04:33:00Z
description = "Course project for Computer Graphics (CS488)"
draft = false
slug = "cs488-final-project"
tags = ["CS488", "project", "ray tracer", "RBD"]
title = "Ray Tracer Project"
institution = "University of Waterloo"
thumb = "/cs488/img/rube_thumb.png"
+++

I implemented a ray tracer with rigid body dynamics for my final CS488 project. Unfortunately I didn't have time to complete collision detection for all primitives, and could only demonstrate colliding spheres.

## Ray Tracer with Rigid Body Dynamics
### Table of Contents
  1. [Dynamic Objects](#1)
  2. [Rigid Body Collisions](#2)
  3. [Texture Mapping](#3)
  4. [Bump Mapping](#4)
  5. [Multi-threaded Rendering](#5)
  6. [Intensity Threshold Optimization](#6)
  7. [Antialiasing](#7)
  8. [Additional Primitives](#8)
  9. [Constructive Solid Geometry](#9)
  10. [Rube Goldberg machine](#10)

### <a name="1"></a><a name="2"></a> Objective 1 & 2: Dynamic Objects

[Example of two spheres colliding](/cs488/vid/dynamics_demo.mpg)

### <a name="3"></a> Objective 3: Texture Mapping

The images show texture mapping for all primitives, a simple mesh (background) and a CSG object.

<img src="/cs488/img/texture_demo_no_alias.png">
No antialiasing, but with bilinear interpolation.

<img src="/cs488/img/texture_demo_no_alias_bilin.png">
No antialiasing or bilinear interpolation.

<img src="/cs488/img/texture_demo.png">
All features enabled.

### <a name="4"></a> Objective 4: Bump Mapping

<img src="/cs488/img/bump_demo.png">
Bump mapping for all primitives, a simple mesh (background) and a CSG object.

### <a name="5"></a> Objective 5: Multi-threaded Rendering
The final scene (see [Objective 10](#10)), which has one frame, is rendered with a varying number of threads. All features of the ray tracer were enabled. The following renders were performed with the unix "time" command, on the following machine:
`gl25.student.cs Asus based AMD quad core, 4gb ram, 2x500g hd, Asus ENGTX260GL`

| # of Threads | real    | user     | sys     |
| :----------- | :------ | :------- | :------ |
| n = 1        | 274.23s | 273.321s | 0.704s  |
| n = 5        | 108.91s | 269.028s | 0.669s  |
| n = 10       | 93.64s  | 268.232s | 0.776s  |
| n = 20       | 81.41s  | 270.123s | 0.732s  |
| n = 50       | 98.66s  | 301.526s | 61.047s |

Thus running the ray tracer with 20 threads speeds up the run time by 3.37 times, or by 3 minutes and 12 seconds, which is a significant improvement.

### <a name="6"></a>Objective 6: Intensity Threshold Optimization
I used my final scene to demonstrate the gain of the intensity threshold optimization, which actually showed a significant increase in performance.

Antialiasing was disabled, the reflection depth was set at 5, and 4 threads was used. The threshold used was large value of 0.000001. The following renders were performed with the unix "time" command, on the following machine

`gl25.student.cs Asus based AMD quad core, 4gb ram, 2x500g hd, Asus ENGTX260GL`

|      | Threshold | No Threshold |
| ---- | :-------- | :----------- |
| real | 16.90s    | 40.13s       |
| user | 39.094s   | 131.640s     |
| sys  | 0.568s    | 0.608s       |

Thus we have found a significant increase in performance (~ 2.5 times) by using the intensity threshold check.  Note that without the check, 1752362 more shader computations were made.

### <a name="7"></a>Objective 7: Anti-aliasing
The given scene (slightly modified) from CS488 Assignment 4 with polyhedral cows is used to demonstrate antialiasing

<img src="/cs488/img/macho_no_alias.png">
The original scene with anti-aliasing disabled

<img src="/cs488/img/macho_hi.png">
The red shows where the anti-aliasing algorithm was used to smooth edges.

<img src="/cs488/img/macho_alias.png">
Anti-aliasing enabled.

<img src="/cs488/img/macho_rand.png">
A randomized version of the same algorithm

### <a name="8"></a>Objective 8: Additional Primitives
See Objectives [3](#3) and [4](#4) for examples of implemented primitives.

### <a name="9"></a>Objective 9: Constructive Solid Geometry
<img src="/cs488/img/csg_demo.png">
A sequence of two spheres with union, intersection, and difference. In addition a more complex CSG model is also provided: The cup is two cylinders subtracted, and intersected with two spheres to provide rounded edges. The half torus was generated by instancing multiple cylinders and taking their union.

### <a name="10"></a>Objective 10: Rube Goldberg machine
<img src="/cs488/img/rube.png">
A sample image of the proposed Rube Goldberg machine.
