+++
author = "Egor Larionov"
authors = ["Yifei Li", "Hsiao-yu Chen", "Egor Larionov", "Nikolaos Sarafianos", "Wojciech Matusik", "Tuur Stuyck"]
date = 2024-03-29
journal = "IEEE/CVF Conference on Computer Vision and Pattern Recognition (CVPR)"
description = ""
draft = false
slug = "diff-avatar"
title = "DiffAvatar: Simulation-Ready Garment Optimization with Differentiable Simulation"
tags = ["graphics", "physical simulation", "digital avatar", "differentiable simulation", "cloth simulation"]
categories = ["publication"]
thumb = "/img/diff-avatar-thumb.png"
pdf = "https://arxiv.org/pdf/2311.12194"
+++

![DiffAvatar Teaser](/img/diff-avatar-teaser.png)

 The realism of digital avatars is crucial in enabling telepresence applications with self-expression and customization. While physical simulations can produce realistic motions for clothed humans, they require high-quality garment assets with associated physical parameters for cloth simulations. However, manually creating these assets and calibrating their parameters is labor-intensive and requires specialized expertise. Current methods focus on reconstructing geometry, but don't generate complete assets for physics-based applications. To address this gap, we propose DiffAvatar, a novel approach that performs body and garment co-optimization using differentiable simulation. By integrating physical simulation into the optimization loop and accounting for the complex nonlinear behavior of cloth and its intricate interaction with the body, our framework recovers body and garment geometry and extracts important material parameters in a physically plausible way. Our experiments demonstrate that our approach generates realistic clothing and body shape suitable for downstream applications.

| | |
| ----: |:---- |
| Paper | [PDF](https://arxiv.org/pdf/2311.12194) |
| Project Page | [MIT](https://people.csail.mit.edu/liyifei/publication/diffavatar/) |
| | |

## BibTeX

```
@inproceedings{li2023diffavatar,
  title = {
    {DiffAvatar}:
    Simulation-Ready Garment Optimization with Differentiable Simulation
  },
  author = {
    Li, Yifei  and
    Chen, Hsiao-yu and
    Larionov, Egor and
    Sarafianos, Nikolaos and
    Matusik, Wojciech and
    Stuyck, Tuur
  },
  booktitle = {
    Proceedings of the IEEE/CVF Conference on
    Computer Vision and Pattern Recognition (CVPR)
  },
  month = {June},
  eprint={2311.12194},
  year = {2024}
}
```