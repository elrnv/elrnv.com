+++
author = "Egor Larionov"
authors = ["Egor Larionov"]
journal = "PhD Thesis (University of British Columbia)"
date = 2022-12-31
draft = false
slug = "friction-and-differentiable-simulation"
title = "Constrained dynamics with frictional contact on smooth surfaces"
categories = ["thesis", "publication"]
thumb = "/img/egor_larionov_thesis_thumb.png"
pdf = "/phd/egor_larionov_thesis.pdf"
code = "https://github.com/elrnv/softy.git"
+++

Friction and contact pose a great challenge to efficient and accurate
simulation of deformable objects for computer graphics and engineering
applications.  In contrast to many engineering applications, simulation software
for graphics often permits larger approximation errors in favour of better
predictability, controllability and efficiency.
This dissertation explores modern methods for frictional contact resolution in
computer graphics.  In particular, the focus is on offline simulation of smooth
elastic objects subject to contact with other elastic solids and cloth.  We
explore traditional non-smooth friction formulations as well as smoothed
frictional contact, which lends itself well to differentiable simulation and
analysis.  We then explore a particular application of differentiable simulation
to motivate the direction of research.

In graphics, even smooth objects are typically approximated using piecewise
linear polyhedra, which exhibit sliding artifacts that can be interpreted as
artificial friction making simulations less predictable.  We develop a technique
for improving fidelity of sliding contact between smooth objects.

Frictional contacts are traditionally resolved using non-smooth models, which
are complex to analyse and difficult to compute to a desirable error estimate.
We propose a unified description of the equations of motion subject to
frictional contacts using a smooth model that converges to an accurate friction
response. We further analyse the implications of this formulation and compare
our results to state-of-the-art methods.

The smooth model uniquely resolves frictional contacts, while also being fully
differentiable.  This allows inverse problems using our formulation to be solved
by gradient-based methods.  We begin our exploration of differentiable
simulation applications with a parameter estimation task.  Elastic parameters
are estimated for a three distinct cloth materials using a novel capture,
registration and estimation pipeline. Static equilibrium cloth configurations
are efficiently estimated using a popular compliant constraint dynamics.  In
this work we address a common issue of bifurcation in cloth, which causes final
configuration mismatches during estimation.  Finally, we postulate an extension
to compliant constraint dynamics using our friction model, to show how our
previous work can be used in parameter estimation tasks involving contact and
friction.

| | |
| ----: |:---- |
| Paper | [PDF](/phd/egor_larionov_thesis.pdf) |
| Publication | [UBC](https://open.library.ubc.ca/soa/cIRcle/collections/ubctheses/24/items/1.0422992) |
| Code | [GitHub](https://github.com/elrnv/softy.git)
| | |

# BibTeX

```
@phdthesis{larionov22phd,
  series = {Electronic Theses and Dissertations (ETDs) 2008+},
  title = {Constrained dynamics with frictional contact on smooth surfaces},
  url = {https://open.library.ubc.ca/collections/ubctheses/24/items/1.0422992},
  DOI = {http://dx.doi.org/10.14288/1.0422992}, school={University of British Columbia},
  author = {Larionov, Egor},
  year = {2022},
  collection = {Electronic Theses and Dissertations (ETDs) 2008+}
}
```

