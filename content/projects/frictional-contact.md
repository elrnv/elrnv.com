+++
author = "Egor Larionov"
authors = ["Egor Larionov", "Ye Fan", "Dinesh K. Pai"]
date = 2021-04-29
journal = "ACM Transactions on Graphics"
linktitle = "Frictional Contact"
draft = false
slug = "frictional-contact-on-smooth-elastic-solids"
title = "Frictional Contact on Smooth Elastic Solids"
thumb = "/img/fcses_tog2021.png"
categories = ["publication"]
pdf = "/fcses_tog21.pdf"
video = "/vid/fcses_tog2021.webm"
code = "https://github.com/elrnv/softy/tree/tog2021"
+++

![Frictional Contact Teaser](/img/fcses_banner.png)

Frictional contact between deformable elastic objects remains a difficult simulation problem in
computer graphics. Traditionally, contact has been resolved using sophisticated collision detection
schemes and methods that build on the assumption that contact happens between polygons. While
polygonal surfaces are an efficient representation for solids, they lack some intrinsic properties
that are important for contact resolution. Generally, polygonal surfaces are not equipped with an
intrinsic inside and outside partitioning or a smooth distance field close to the surface.

Here we propose a new method for resolving frictional contacts against deforming implicit surface
representations that addresses these problems. We augment a moving least squares (MLS) implicit
surface formulation with a local kernel for resolving contacts, and develop a simple parallel
transport approximation to enable transfer of frictional impulses. Our variational formulation of
dynamics and elasticity enables us to naturally include contact constraints, which are resolved as
one Newton-Raphson solve with linear inequality constraints. We extend this formulation by
forwarding friction impulses from one time step to the next, used as external forces in the
elasticity solve. This maintains the decoupling of friction from elasticity thus allowing for
different solvers to be used in each step. In addition, we develop a variation of staggered
projections, that relies solely on a non-linear optimization without constraints and does not
require a discretization of the friction cone. Our results compare favorably to a popular industrial
elasticity solver (used for visual effects), as well as recent academic work in frictional contact,
both of which rely on polygons for contact resolution. We present examples of coupling between rigid
bodies, cloth and elastic solids.


| | |
| ----: |:---- |
| Paper | [PDF](/fcses_tog21.pdf) |
| Project Page | [ACM](https://dl.acm.org/doi/10.1145/3446663) |
| Video   | [WEBM](/vid/fcses_tog2021.webm) |
| Code | [GitHub](https://github.com/elrnv/softy/tree/tog2021) |
| | |

## BibTeX

```
@article{lfp21,
    author = {Larionov, Egor and Fan, Ye and Pai, Dinesh K.},
    title = {Frictional Contact on Smooth Elastic Solids},
    year = {2021},
    issue_date = {April 2021},
    publisher = {Association for Computing Machinery},
    address = {New York, NY, USA},
    volume = {40},
    number = {2},
    issn = {0730-0301},
    url = {https://doi.org/10.1145/3446663},
    doi = {10.1145/3446663},
    journal = {ACM Trans. Graph.},
    month = apr,
    articleno = {15},
    numpages = {17},
    keywords = {contact, elasticity, dry friction}
}
```
