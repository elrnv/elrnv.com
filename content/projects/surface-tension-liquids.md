+++
author = "Egor Larionov"
authors = ["Egor Larionov"]
journal = "Master's Thesis (University of Waterloo)"
date = 2016-07-10T18:48:39Z
description = ""
draft = false
slug = "surface-tension-liquids"
title = "2D Surface Tension Liquids"
categories = ["thesis", "publication"]
thumb = "/img/masters_thesis_thumbnail.png"
pdf = "/masters_thesis.pdf"
video = "https://vimeo.com/174123728"
code = "https://github.com/elrnv/mfd-notebooks"
+++

Liquid simulation has been an interest of mine for some time now.  Water, maybe the most ubiquitous
liquid familiar to us, exhibits many fascinating visual properties.  This makes water simulation a
very hot topic in the visual effects industry.

In the history of liquid simulation, there hasn't been one single efficient, clear cut method for
simulating water in all its capacity.  Each method has its advantages and disadvantages.  In the
visual effects industry, prominent methods for liquid simulation are grid-based (e.g. Houdini,
Blender).  These methods tend to have poor support for stable surface forces, because they typically
lack an explicit surface representation.

To remedy this issue, I developed a method for liquid simulation that couples with existing
grid-based methods and can exhibit stable surface tension effects, under the supervision of
[Christopher Batty](https://cs.uwaterloo.ca/~c2batty/). This project took just over a year to
complete and resulted in my Master's thesis titled ["The Mimetic Approach to Incompressible Surface
Tension Flows"](/masters_thesis.pdf):

[![The Mimetic Approach to Incompressible Surface Tension Flows](/img/thesis_thumbnail.png)](/masters_thesis.pdf)

It can also be found on the [university repository](https://uwspace.uwaterloo.ca/handle/10012/10410).

The following supplemental video outlines various results I was able to achieve in 2D:

<div class='embed-container'>
<iframe src="https://player.vimeo.com/video/174123728" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
</div>
<p><a href="https://vimeo.com/174123728">The Mimetic Approach to Incompressible Surface Tension Flows</a> from <a href="https://vimeo.com/user54225153">Egor Larionov</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

A number of jupyter notebooks with Julia code used for numerical studies related to MFD can be
found on [github](https://github.com/elrnv/mfd-notebooks).

# BibTeX

```
@mastersthesis{larionov2016mimetic,
  title={The Mimetic Approach to Incompressible Surface Tension Flows},
  author={Larionov, Egor},
  year={2016},
  school={University of Waterloo}
}
```