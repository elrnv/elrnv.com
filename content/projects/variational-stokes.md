+++
author = "Egor Larionov"
authors = ["Egor Larionov*", "Christopher Batty*", "Robert Bridson"]
author_note = "(*Joint first authors)"
date = 2017-07-30
journal = "ACM Transactions on Graphics (SIGGRAPH)"
linktitle = "Variational Stokes"
description = ""
draft = false
slug = "variational-stokes-a-unified-pressure-viscosity-solver-for-accurate-viscous-liquids"
title = "Variational Stokes: A Unified Pressure-Viscosity Solver for Accurate Viscous Liquids"
thumb = "/img/variational_stokes_thumb.png"
categories = ["publication"]
pdf = "http://poisson.cs.uwaterloo.ca/stokes/stokes.pdf"
video = "https://www.youtube.com/watch?v=QJePco0_os8"
code = "http://github.com/elrnv/stokes-houdini"
+++

![Variational Stokes Teaser Figure](/img/variational_stokes_banner.png)

We propose a novel unsteady Stokes solver for coupled viscous and pressure forces in grid-based
liquid animation which yields greater accuracy and visual realism than previously achieved. Modern
fluid simulators treat viscosity and pressure in separate solver stages, which reduces accuracy and
yields incorrect free surface behavior. Our proposed implicit variational formulation of the Stokes
problem leads to a symmetric positive definite linear system that gives properly coupled forces,
provides unconditional stability, and treats difficult boundary conditions naturally through simple
volume weights. Surface tension and moving solid boundaries are also easily incorporated.
Qualitatively, we show that our method recovers the characteristic rope coiling instability of
viscous liquids and preserves fine surface details, while previous grid-based schemes do not.
Quantitatively, we demonstrate that our method is convergent through grid refinement studies on
analytical problems in two dimensions. We conclude by offering practical guidelines for choosing an
appropriate viscous solver, based on the scenario to be animated and the computational costs of
different methods.

<p>
<a href="https://www.acm.org/publications/policies/artifact-review-badging#functional"><img class="acm-badge" style="float:left" src="https://www.acm.org/binaries/content/gallery/acm/publications/large-replication-badges/artifacts_evaluated_functional.jpg"/></a>
<a href="https://www.acm.org/publications/policies/artifact-review-badging#replicated"><img class="acm-badge" src="https://www.acm.org/binaries/content/gallery/acm/publications/large-replication-badges/results_replicated.jpg"/></a>
</p>

| | |
| ----: |:---- |
| Paper   | [PDF](http://poisson.cs.uwaterloo.ca/stokes/stokes.pdf) |
| Project Page | [ACM](https://dl.acm.org/doi/10.1145/3072959.3073628), [UW](http://poisson.cs.uwaterloo.ca/stokes/index.html) |
| Trailer | [YouTube](https://www.youtube.com/watch?v=4ongq0IVx1A) |
| Video   | [MP4](http://poisson.cs.uwaterloo.ca/stokes/stokes.mp4), [YouTube](https://www.youtube.com/watch?v=QJePco0_os8) |
| Code | [GitHub](https://github.com/elrnv/stokes-houdini) |
| Supplemental Notes | [PDF](http://poisson.cs.uwaterloo.ca/stokes/supplemental.pdf) |
| Supplemental Video | [MP4](http://poisson.cs.uwaterloo.ca/stokes/supplemental.mp4) |
| | |

## BibTeX

```
@article{lbb17,
    author = {
        Larionov, Egor and
        Batty, Christopher and
        Bridson, Robert
    },
    title = {
        Variational Stokes:
        A Unified Pressure-Viscosity Solver
        for Accurate Viscous Liquids
    },
    year = {2017},
    issue_date = {July 2017},
    publisher = {Association for Computing Machinery},
    address = {New York, NY, USA},
    volume = {36},
    number = {4},
    issn = {0730-0301},
    url = {https://doi.org/10.1145/3072959.3073628},
    doi = {10.1145/3072959.3073628},
    journal = {ACM Trans. Graph.},
    month = jul,
    articleno = {Article 101},
    numpages = {11},
    keywords = {liquids, viscosity, free surfaces, stokes}
}
```
