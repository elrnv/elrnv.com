+++
author = "Egor Larionov"
categories = ["course", "project"]
institution = "University of Waterloo"
date = 2013-12-09
description = "Course project for Artificial Intelligence (CS686)"
draft = false
slug = "cs686-ai-project"
tags = ["ai", "particles"]
title = "Experiment with 2D Particle Simulation and Artificial Neural Networks"
thumb = "/cs686/cs686proj_thumb.png"
pdf = "/cs686/cs686proj.pdf"
code = "/cs686/cs686proj.tar.gz"
+++

In an effort to learn more about artificial neural networks, I implemented a simple method to
learn the collision response between a pair of circular particles with a constant radius.
Although not practically useful, this exercise was enlightening. The details can be found in the
writeup:

[![2D Particle Simulation with Feed-Forward Neural Networks](/cs686/cs686proj_paper_thumb.png)](/cs686/cs686proj.pdf)
