+++
author = "Egor Larionov"
authors = ["Pearson A. Wyder-Hodge", "Egor Larionov", "Dinesh K. Pai"]
date = 2022-09-01
journal = "Tribology International"
linktitle = "In situ measurement of friction on curved surfaces"
description = ""
draft = false
slug = "in-situ-measurement-of-friction-on-curved-surfaces"
title = "In situ measurement of friction on curved surfaces"
thumb = "/img/friction_measurement_on_curved_surfaces_thumb.png"
categories = ["publication"]
pdf = "/friction_measurement_on_curved_surfaces.pdf"
+++

In situ measurement of frictional properties, particularly of human skin, is challenging. A major
challenge is that the measured surface may be curved and oriented arbitrarily relative to the
measurement device, making it difficult to obtain accurate and reliable friction estimates.

Here we propose a method to simultaneously estimate the orientation of the surface during friction
measurements, using a custom-made hand-held tribometer. The method accounts for the position and
orientation of the tribometer relative to the shape of the measured surface. Our simple calibration
method allows lightweight tribometers to be built with cheaper materials like plastic, without
sacrificing accuracy.


| | |
| ----: |:---- |
| Preprint | [PDF](/friction_measurement_on_curved_surfaces.pdf) |
| Publication | [Elsevier](https://www.sciencedirect.com/science/article/abs/pii/S0301679X22001645) |
| | |

## BibTeX

```
@article{whlp22,
    title = {In situ measurement of friction on curved surfaces},
    journal = {Tribology International},
    volume = {173},
    pages = {107591},
    year = {2022},
    issn = {0301-679X},
    doi = {https://doi.org/10.1016/j.triboint.2022.107591},
    url = {https://www.sciencedirect.com/science/article/pii/S0301679X22001645},
    author = {Pearson A. Wyder-Hodge and Egor Larionov and Dinesh K. Pai},
    keywords = {Friction, Measurement, Sliding, Topography},
}
```
