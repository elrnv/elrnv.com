+++
author = "Egor Larionov"
authors = ["Egor Larionov", "Andreas Longva", "Uri M. Ascher", "Jan Bender", "Dinesh K. Pai"]
date = 2022-11-24
year = 2024
journal = "IEEE Transactions on Visualization and Computer Graphics (TVCG)"
linktitle = "Implicit Frictional Dynamics"
draft = false
slug = "implicit-frictional-dynamics"
title = "Implicit Frictional Dynamics with Soft Constraints"
thumb = "/img/implicit-frictional-dynamics-thumb.jpg"
categories = ["publication"]
pdf = "https://arxiv.org/pdf/2211.10618"
video = "/vid/implicit-frictional-dynamics.webm"
code = "https://github.com/elrnv/softy/tree/tvcg2024"
+++

![Implicit Frictional Dynamics Teaser](/img/implicit-frictional-dynamics-teaser.png)

Dynamics simulation with frictional contacts is important for a wide range of
applications, from cloth simulation to object manipulation. Recent methods
using smoothed lagged friction forces have enabled robust and differentiable
simulation of elastodynamics with friction. However, the resulting frictional
behavior can be inaccurate and may not converge to analytic
solutions.
Here we evaluate the accuracy of lagged friction models
in comparison with implicit frictional contact systems.
We show that major inaccuracies near
the stick-slip threshold in such systems are caused by lagging of friction
forces rather than by smoothing the Coulomb friction curve.
Furthermore, we demonstrate how systems involving implicit or lagged
friction can be correctly used with higher-order time integration and
highlight limitations in earlier attempts.
We demonstrate how to exploit forward-mode automatic differentiation to
simplify and, in some cases, improve the performance of the inexact Newton
method.
Finally, we show that other complex phenomena can also be simulated
effectively while maintaining smoothness of the entire system.  We extend our
method to exhibit stick-slip frictional behavior and preserve volume on
compressible and nearly-incompressible media using soft constraints.

| | |
| ----: |:---- |
| Paper | [PDF](https://arxiv.org/pdf/2211.10618) |
| Code | [GitHub](https://github.com/elrnv/softy/tree/tvcg2024) |
| Video | [WEBM](/vid/implicit-frictional-dynamics.webm), [YouTube](https://youtu.be/vpv99doOdGs) |
| | |

## BibTeX

```
@misc{larionov2022implicitfriction,
  title={Implicit frictional dynamics with soft constraints},
  author={
    Egor Larionov and
    Andreas Longva and
    Uri M. Ascher and
    Jan Bender and
    Dinesh K. Pai
  },
  year={2022},
  eprint={2211.10618},
  archivePrefix={arXiv},
  primaryClass={cs.GR},
  url={https://arxiv.org/abs/2211.10618},
}
```
