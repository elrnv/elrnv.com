+++
author = "Egor Larionov"
authors = ["Dinesh K. Pai", "Austin Rothwell", "Pearson Wyder-Hodge", "Alistair Wick", "Ye Fan",  "Egor Larionov", "Darcy Harrison", "Debanga R. Neog", "Cole Shing"]
date = 2018-08-12
journal = "ACM Transactions on Graphics (SIGGRAPH)"
linktitle = "Human Touch"
description = ""
draft = false
slug = "human-touch"
title = "The Human Touch: Measuring Contact with Real Human Soft Tissues"
thumb = "/img/human_touch_thumb.png"
categories = ["publication"]
pdf = "https://www.cs.ubc.ca/research/HumanTouch/HumanTouchSIGGRAPH2018.pdf"
video = "https://www.youtube.com/watch?time_continue=3&v=redCdmnDZwY&feature=emb_logo"
+++

Simulating how the human body deforms in contact with external objects, tight clothing, or other
humans is of central importance to many fields. Despite great advances in numerical methods, the
material properties required to accurately simulate the body of a real human have been sorely
lacking. Here we show that mechanical properties of the human body can be directly measured using a
novel hand-held device. We describe a complete pipeline for measurement, modeling, parameter
estimation, and simulation using the finite element method. We introduce a phenomenological model
(the sliding thick skin model) that is effective for both simulation and parameter estimation. Our
data also provide new insights into how the human body actually behaves. The methods described here
can be used to create personalized models of an individual human or of a population. Consequently,
our methods have many potential applications in computer animation, product design, e-commerce, and
medicine.

| | |
| ----: |:---- |
| Paper        | [PDF](https://www.cs.ubc.ca/research/HumanTouch/HumanTouchSIGGRAPH2018.pdf) |
| Project Page | [ACM](https://dl.acm.org/doi/10.1145/3197517.3201296) |
| Video        | [YouTube](https://www.youtube.com/watch?time_continue=3&v=redCdmnDZwY&feature=emb_logo) |
| | |

## BibTeX

```
@article{prwwflhds18,
    author = {
        Pai, Dinesh K. and
        Rothwell, Austin and
        Wyder-Hodge, Pearson and
        Wick, Alistair and
        Fan, Ye and
        Larionov, Egor and
        Harrison, Darcy and
        Neog, Debanga Raj and
        Shing, Cole
    },
    title = {
        The Human Touch:
        Measuring Contact with Real Human Soft Tissues
    },
    year = {2018},
    issue_date = {August 2018},
    publisher = {Association for Computing Machinery},
    address = {New York, NY, USA},
    volume = {37},
    number = {4},
    issn = {0730-0301},
    url = {https://doi.org/10.1145/3197517.3201296},
    doi = {10.1145/3197517.3201296},
    journal = {ACM Trans. Graph.},
    month = jul,
    articleno = {Article 58},
    numpages = {12},
    keywords = {
        contact, finite element, soft tissues, biomechanics,
        measurement, human simulation
    }
}
```
