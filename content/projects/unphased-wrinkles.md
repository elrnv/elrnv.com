+++
author = "Egor Larionov"
authors = ["Egor Larionov", "Marie-Lena Eckert", "Katja Wolff", "Tuur Stuyck"]
date = 2022-12-16
journal = "arXiv"
linktitle = "Unphased Wrinkles"
draft = false
slug = "unphased-wrinkles"
title = "Unphased Wrinkles: Estimating cloth elasticity parameters using a frequency-based loss"
thumb = "/img/unphased-wrinkles-thumb.jpg"
categories = ["publication"]
pdf = "https://arxiv.org/abs/2212.08790"
+++

![Unphased Wrinkles Teaser](/img/unphased-wrinkles-teaser.png)

Generating realistic clothing for virtual applications like online retail and digital avatars is crucial but requires expert knowledge of 3D tools to generating believable simulations.  Recently, a number of works proposed to estimate cloth material properties from specialized capture setups. However, these systems tend to be monolithic, complex and expensive. We propose a simplified method for automatically determining parameters based on easily captured real-world fabrics.  While existing methods carefully design experiments to isolate stretch parameters from bending modes, we embrace that stretching fabrics causes wrinkling and propose a novel specialized loss for comparing wrinkled fabrics.  We designed our objective function to capture material-specific behavior, resulting in similar values for different wrinkle configurations of the same material. We estimate bending first, given that membrane stiffness has little effect on bending. We use differentiable simulation to find an optimal set of parameters that minimizes the difference between simulated cloth and deformed target cloth. Furthermore, our pipeline decouples the capture method from the optimization by registering a template mesh to the scanned data. These choices simplify the capture system and allow for wrinkles in scanned fabrics.  We demonstrate our method on captured data of three different real-world fabrics and on three digital fabrics produced by a third-party simulator.

| | |
| ----: |:---- |
| Paper | [PDF](https://arxiv.org/abs/2212.08790) |
| | |

## BibTeX

```
@misc{larionov2022estimatingclothelasticityparameters,
      title={Estimating Cloth Elasticity Parameters Using Position-Based Simulation of Compliant Constrained Dynamics}, 
      author={Egor Larionov and Marie-Lena Eckert and Katja Wolff and Tuur Stuyck},
      year={2022},
      eprint={2212.08790},
      archivePrefix={arXiv},
      primaryClass={cs.GR},
      url={https://arxiv.org/abs/2212.08790}, 
}
```
