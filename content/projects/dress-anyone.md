+++
author = "Egor Larionov"
authors = ["Hsiao-yu Chen", "Egor Larionov", "Ladislav Kavan", "Gene Lin", "doug Roble", "Olga Sorkine-Hornung", "Tuur Stuyck"]
date = 2024-05-29
description = "An arXiv paper"
draft = true
slug = "dress-anyone"
title = "Dress Anyone : Automatic Physically-Based Garment Pattern Refitting "
tags = ["graphics", "physical simulation", "digital avatar", "differentiable simulation", "cloth simulation"]
categories = ["publication"]
thumb = "/img/dress-anyone.png"
pdf = "https://arxiv.org/abs/2405.19148"
+++

### Hsiao-yu Chen, Egor Larionov, Ladislav Kavan, Gene Lin, Doug Roble, Olga Sorkine-Hornung, Tuur Stuyck

Well-fitted clothing is essential for both real and virtual garments to enable self-expression and accurate representation for a large variety of body types. Common practice in the industry is to provide a pre-made selection of distinct garment sizes such as small, medium and large. While these may cater to certain groups of individuals that fall within this distribution, they often exclude large sections of the population. In contrast, individually tailored clothing offers a solution to obtain custom-fit garments that are tailored to each individual. However, manual tailoring is time-consuming and requires specialized knowledge, prohibiting the approach from being applied to produce fitted clothing at scale. To address this challenge, we propose a novel method leveraging differentiable simulation for refitting and draping 3D garments and their corresponding 2D pattern panels onto a new body shape, enabling a workflow where garments only need to be designed once, in a single size, and they can be automatically refitted to support numerous body size and shape variations. Our method enables downstream applications, where our optimized 3D drape can be directly ingested into game engines or other applications. Our 2D sewing patterns allow for accurate physics-based simulations and enables manufacturing clothing for the real world. 

| | |
| ----: |:---- |
| Paper | [PDF](https://arxiv.org/abs/2405.19148) |
| | |

## BibTeX

```
@misc{chen2024dressanyone,
      title={Dress Anyone : Automatic Physically-Based Garment Pattern Refitting}, 
      author={Hsiao-yu Chen and Egor Larionov and Ladislav Kavan and Gene Lin and Doug Roble and Olga Sorkine-Hornung and Tuur Stuyck},
      year={2024},
      eprint={2405.19148},
      archivePrefix={arXiv},
      primaryClass={cs.GR},
      url={https://arxiv.org/abs/2405.19148}, 
}
```