+++
author = "Egor Larionov"
authors = ["Andreas Longva", "Fabian Löschner", "José Antonio Fernández-Fernández", "Egor Larionov", "Uri M. Ascher", "Jan Bender" ]
date = 2023-11-24
journal = "ACM Transactions on Graphics"
description = "A study of newton-type solvers for incremental potentials"
draft = false
slug = "pitfalls-of-projection"
title = "Pitfalls of Projection: A study of Newton-type solvers for incremental potentials"
tags = ["graphics", "physical simulation", "newton's method", "line-search"]
categories = ["publication"]
thumb = "/img/pitfalls-thumb.png"
pdf = "https://arxiv.org/pdf/2311.14526"
+++

![Pitfalls of projection Teaser](/img/pitfalls-teaser.png)

Nonlinear systems arising from time integrators like Backward Euler can sometimes be reformulated as optimization problems, known as incremental potentials. We show through a comprehensive experimental analysis that the widely used Projected Newton method, which relies on unconditional semidefinite projection of Hessian contributions, typically exhibits a reduced convergence rate compared to classical Newton’s method. We demonstrate how factors like resolution, element order, projection method, material model and boundary handling impact convergence of Projected Newton and Newton.

Drawing on these findings, we propose the hybrid method Project-on-Demand Newton, which projects only conditionally, and show that it enjoys both the robustness of Projected Newton and convergence rate of Newton.  We additionally introduce Kinetic Newton, a regularization-based method that takes advantage of the structure of incremental potentials and avoids projection altogether. We compare the four solvers on hyperelasticity and contact problems.

We also present a nuanced discussion of convergence criteria, and propose a new acceleration-based criterion that avoids problems associated with existing residual norm criteria and is easier to interpret. We finally address a fundamental limitation of the Armijo backtracking line search that occasionally blocks convergence, especially for stiff problems. We propose a novel parameter-free, robust line search technique to eliminate this issue

| | |
| ----: |:---- |
| Paper | [PDF](https://arxiv.org/pdf/2311.14526) |
| Project Page | Coming soon |
| Video | Coming soon |
| | |

## BibTeX

```
@misc{longva2023pitfalls,
  title = {
    Pitfalls of Projection: A study of Newton-type solvers for incremental potentials
  }, 
  author = {
    Andreas Longva and
    Fabian Löschner and
    José Antonio Fernández-Fernández and
    Egor Larionov and
    Uri M. Ascher and
    Jan Bender
  },
  year = {2023},
  eprint = {2311.14526},
  archivePrefix = {arXiv},
  primaryClass = {cs.GR},
  url = {https://arxiv.org/abs/2311.14526}, 
}
```