+++
author = "Egor Larionov"
authors = ["Joy Xiaoji Zhang", "Gene Wei-Chin Lin", "Lukas Bode", "Hsiao-Yu Chen", "Tuur Stuyck", "Egor Larionov"]
date = 2024-01-26
journal = "ACM SIGGRAPH Conference on Motion, Interaction, and Games (MIG)"
description = ""
draft = false
slug = "yarn-cloth"
title = "Estimating Cloth Elasticity Parameters From Homogenized Yarn-Level Models"
tags = ["graphics", "physical simulation", "yarn simulation", "cloth simulation"]
categories = ["publication"]
thumb = "/img/yarn-cloth-thumb.png"
pdf = "https://arxiv.org/abs/2401.15169"
video = "https://youtu.be/bjNjxqX34AE?si=cTdjLZAwqyc6DSWm"
+++

![Cloth elasticity from yarn-level models Teaser](/img/yarn-cloth-teaser.jpg)

Using knowledge of fabric yarn structure and material properties, we propose a forward estimation pipeline to generate realistic shell-level simulations with automatically determined material parameters. For each real-world fabric, we start by observing the underlying yarn structure and derive the yarn model parameters from standard estimates of the Young's moduli as well as simple measurements of the fabric. After sampling a range of in-plane and out-of-plane deformations, we tile the yarns periodically on each deformed surface, simulate and record the yarn responses, which are subsequently used for optimizing the membrane and bending elasticity parameters of a shell model. We validate our results by comparing against real fabric captures and measurements. Finally, we simulate full-body garments and compare to reference footage.

| | |
| ----: |:---- |
| Paper | [PDF](https://dl.acm.org/doi/pdf/10.1145/3677388.3696340), [arXiv](https://arxiv.org/abs/2401.15169) |
| Project Page | [ACM](https://dl.acm.org/doi/10.1145/3677388.3696340) |
| Video | [YouTube](https://youtu.be/bjNjxqX34AE?si=cTdjLZAwqyc6DSWm) |
| | |

<div class="iframe-container">
<iframe class="responsive-iframe" src="https://www.youtube.com/embed/bjNjxqX34AE?si=Y2kyv4wTr1qjqi61" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
</div>

## BibTeX

```
@inproceedings{zhang2024yarncloth,
  author = {
    Zhang, Joy Xiaoji and
    Lin, Gene Wei-Chin and
    Bode, Lukas and
    Chen, Hsiao-Yu and
    Stuyck, Tuur and
    Larionov, Egor
  },
  title = {
    Estimating Cloth Elasticity Parameters From Homogenized Yarn-Level Models
  },
  year = {2024},
  isbn = {9798400710902},
  publisher = {Association for Computing Machinery},
  address = {New York, NY, USA},
  url = {https://doi.org/10.1145/3677388.3696340},
  doi = {10.1145/3677388.3696340},
  booktitle = {
    Proceedings of the 17th ACM SIGGRAPH Conference on
    Motion, Interaction, and Games
  },
  articleno = {7},
  numpages = {12},
  keywords = {
    homogenization,
    physics-based animation,
    yarn-level cloth simulation
  },
  location = {Arlington, VA, USA},
  series = {MIG '24}
}

```