+++
author = "Egor Larionov"
authors = ["Pearson A. Wyder-Hodge", "Egor Larionov", "Dinesh K. Pai"]
date = 2023-06-01
journal = "Tribology International"
linktitle = "In situ measurement of friction on the human body"
description = ""
draft = false
slug = "in-situ-measurement-of-friction-on-the-human-body"
title = "In situ measurement of friction on the human body"
thumb = "/img/friction_measurement_on_the_human_body_thumb.png"
categories = ["publication"]
pdf = "/friction_measurement_on_the_human_body.pdf"
+++

Measuring friction on the human body is challenging on areas where the surface of the skin is
curved. Handheld devices for measuring friction may make it easier to rapidly measure from
hard-to-reach areas, however, the orientation of the probe relative to the surface is often
suboptimal to reliably measure the normal force.

Here, the friction on the surfaces of a mannequin and a human body are measured and corrected using
a curvature correction technique. This is accomplished by accounting for the shape of the
measurement surface and the orientation of the measurement device relative to that surface. Two
strategies for constructing the surface shape of the measurement surface are compared.

| | |
| ----: |:---- |
| Preprint | [PDF](/friction_measurement_on_the_human_body.pdf) |
| Publication | [Elsevier](https://www.sciencedirect.com/science/article/abs/pii/S0301679X23002347) |
| | |

## BibTeX

```
@article{whlp23,
    title = {In situ measurement of friction on the human body},
    journal = {Tribology International},
    volume = {184},
    pages = {108447},
    year = {2023},
    issn = {0301-679X},
    doi = {https://doi.org/10.1016/j.triboint.2023.108447},
    url = {https://www.sciencedirect.com/science/article/pii/S0301679X23002347},
    author = {Pearson A. Wyder-Hodge and Egor Larionov and Dinesh K. Pai},
    keywords = {Friction, Measurement, Sliding, Topography},
}
```
